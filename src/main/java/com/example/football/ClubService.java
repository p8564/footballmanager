package com.example.football;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClubService {

    private List<Club> clubs = new ArrayList<>();

    public List<Club> addTeam(Club club){
        clubs.add(club);
        return clubs;
    }

    public List<Club> getClubs() {
        return clubs;
    }

    public void setClubs(List<Club> clubs) {
        this.clubs = clubs;
    }
}
