package com.example.football;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ClubController {

    private ClubService clubService = new ClubService();

    @GetMapping("/home")
    public String welcomePage(Model model) {

        model.addAttribute("club", new Club());
        return "homepage";
    }

    @PostMapping("/clubs")
    public String clubSubmit(@ModelAttribute Club club, Model model){
//        psvEindhoven.setClubName("PSV Eindhoven");
//        psvEindhoven.setCity("Eindhoven");
//        psvEindhoven.setFoundationYear(1913);
//        psvEindhoven.setDivision("I Division");
        clubService.addTeam(club);
        model.addAttribute("clubs",clubService.getClubs());
        return "clubs";
    }

}
